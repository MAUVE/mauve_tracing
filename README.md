# MAUVE Tracing

This package provides the a tracing library for MAUVE as well as tools to make some basic analyse on traces.

MAUVE Tracing is licensed under the
[GNU Lesser General Public License](https://www.gnu.org/licenses/lgpl-3.0).

## Instal Instructions

General instructions are given in the [Toolchain Readme](https://gitlab.com/mauve/mauve_toolchain).

To install only the MAUVE Tracing package:
```
mkdir -p ~/mauve_ws/src
cd ~/mauve_ws
git clone https://gitlab.com/MAUVE/mauve_tracing.git src/mauve_tracing
catkin_make
```

## Documentation

Documentation is available on https://mauve.gitlab.io
