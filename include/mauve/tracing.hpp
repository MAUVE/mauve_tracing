/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Tracing project.
 *
 * MAUVE Tracing is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Tracing is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */

#ifndef MAUVE_TRACING_HPP
#define MAUVE_TRACING_HPP

#include <string>

namespace mauve {

  /** Tracing functions */
  namespace tracing {

    /**
     * Trace the start of a program with a message
     * \param message message of the trace
     */
    void trace_start(const std::string& message);

    /**
     * Trace the end of a program with a message
     * \param message message of the trace
     */
    void trace_end(const std::string& message);

    /**
     * Trace an event with a message
     * \param message message of the trace
     */
    void trace_event(const std::string& message);

    /**
     * Trace a data value as a string
     * \param name the name of the data
     * \param value the value of the data
     */
    void trace_data(const std::string& name, const std::string& value);
    
  }

}

#endif
