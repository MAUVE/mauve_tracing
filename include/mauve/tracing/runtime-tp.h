/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Tracing project.
 *
 * MAUVE Tracing is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Tracing is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */

#if !defined(MAUVE_RUNTIME_TP_H) || defined(TRACEPOINT_HEADER_MULTI_READ)
#define MAUVE_RUNTIME_TP_H

#include <lttng/tracepoint.h>


/* Resource services */

TRACEPOINT_EVENT_CLASS(
  mauve,
  resource,
  TP_ARGS(
    const char*, resource_name,
    const char*, service_name
  ),
  TP_FIELDS(
    ctf_string ( resource, resource_name )
    ctf_string ( service, service_name )
  )
)

TRACEPOINT_EVENT_INSTANCE(
  mauve,
  resource,
  service_execution_begin,
  TP_ARGS(
    const char*, resource_name,
    const char*, service_name
  )
)
TRACEPOINT_LOGLEVEL(mauve, service_execution_begin, TRACE_INFO)

TRACEPOINT_EVENT_INSTANCE(
  mauve,
  resource,
  service_execution_end,
  TP_ARGS(
    const char*, resource_name,
    const char*, service_name
  )
)
TRACEPOINT_LOGLEVEL(mauve, service_execution_end, TRACE_INFO)

/* State machine execution */

TRACEPOINT_EVENT_CLASS(
  mauve,
  component,
  TP_ARGS(
    const char*, component_name,
    unsigned long, component_clock,
    const char*, state_name
  ),
  TP_FIELDS(
    ctf_string ( component, component_name )
    ctf_integer ( unsigned long, clock, component_clock )
    ctf_string ( state, state_name )
  )
)

TRACEPOINT_EVENT_INSTANCE(
  mauve,
  component,
  state_execution_begin,
  TP_ARGS(
    const char*, component_name,
    unsigned long, component_clock,
    const char*, state_name
  )
)
TRACEPOINT_LOGLEVEL(mauve, state_execution_begin, TRACE_INFO)

TRACEPOINT_EVENT_INSTANCE(
  mauve,
  component,
  state_execution_end,
  TP_ARGS(
    const char*, component_name,
    unsigned long, component_clock,
    const char*, state_name
  )
)
TRACEPOINT_LOGLEVEL(mauve, state_execution_end, TRACE_INFO)

TRACEPOINT_EVENT_INSTANCE(
  mauve,
  component,
  state_synchronization_begin,
  TP_ARGS(
    const char*, component_name,
    unsigned long, component_clock,
    const char*, state_name
  )
)
TRACEPOINT_LOGLEVEL(mauve, state_synchronization_begin, TRACE_INFO)

TRACEPOINT_EVENT_INSTANCE(
  mauve,
  component,
  state_synchronization_end,
  TP_ARGS(
    const char*, component_name,
    unsigned long, component_clock,
    const char*, state_name
  )
)
TRACEPOINT_LOGLEVEL(mauve, state_synchronization_end, TRACE_INFO)

/* Component start */

TRACEPOINT_EVENT(
  mauve,
  component_start,
  TP_ARGS(
    const char*, component_name,
    unsigned long, component_clock
  ),
  TP_FIELDS(
    ctf_string ( name, component_name )
    ctf_integer ( unsigned long, clock, component_clock )
  )
)
TRACEPOINT_LOGLEVEL(mauve, component_start, TRACE_INFO)

#endif /* MAUVE_TP_H */
