/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Tracing project.
 *
 * MAUVE Tracing is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Tracing is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */

#if !defined(MAUVE_USER_TP_H) || defined(TRACEPOINT_HEADER_MULTI_READ)
#define MAUVE_USER_TP_H

#include <lttng/tracepoint.h>

TRACEPOINT_EVENT_CLASS(
  mauve,
  user,
  TP_ARGS(
    const char*, message
  ),
  TP_FIELDS(
    ctf_string ( message, message )
  )
)

TRACEPOINT_EVENT_INSTANCE(
  mauve,
  user,
  start,
  TP_ARGS(
    const char*, message
  )
)
TRACEPOINT_LOGLEVEL(mauve, start, TRACE_INFO)

TRACEPOINT_EVENT_INSTANCE(
  mauve,
  user,
  end,
  TP_ARGS(
    const char*, message
  )
)
TRACEPOINT_LOGLEVEL(mauve, end, TRACE_INFO)

TRACEPOINT_EVENT_INSTANCE(
  mauve,
  user,
  event,
  TP_ARGS(
    const char*, message
  )
)
TRACEPOINT_LOGLEVEL(mauve, event, TRACE_INFO)

TRACEPOINT_EVENT(
  mauve,
  data,
  TP_ARGS(
    const char*, name,
    const char*, value
  ),
  TP_FIELDS(
    ctf_string ( name, name )
    ctf_string ( value, value )
  )
)
TRACEPOINT_LOGLEVEL(mauve, data, TRACE_INFO)

#endif /* MAUVE_TP_H */
