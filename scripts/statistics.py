#! /usr/bin/env python3

try:
    from babeltrace.reader import TraceCollection
except:
    from babeltrace import TraceCollection

class Statistics:
    def __init__(self):
        self.trace_collection = TraceCollection()
        self.T0 = 0
        self.events = []

    def load_trace(self, trace_path, max_count=0):
        self.trace_collection.add_traces_recursive(trace_path, 'ctf')

    def periodicity(self):
        starts = {}
        for e in self.trace_collection.events:
            if "state_execution_begin" in e.name:
                c = e["name"]
                if not c in starts.keys():
                    starts[c] = []
                starts[c].append(e.timestamp)

        for c, ts in starts.items():
            ts.sort()
            Tmin = -1
            Tmax = -1
            T = 0

            for ti, tj in zip(ts, ts[1:]):
                d = tj - ti
                if Tmin < 0 or Tmin > d: Tmin = d
                if Tmax < d: Tmax = d
                T = T + d

            T = T / len(ts)

            print(c, Tmin, T, Tmax)

if __name__ == "__main__":

    import argparse
    parser = argparse.ArgumentParser(description='Compute periodicity of components from a trace')
    parser.add_argument('trace', type=str, help='path to trace folder')
    args = parser.parse_args()

    trace_path = args.trace

    stats = Statistics()
    stats.load_trace(trace_path)
    stats.periodicity()
