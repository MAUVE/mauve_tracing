/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Tracing project.
 *
 * MAUVE Tracing is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Tracing is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */

#include "mauve/tracing.hpp"

#define TRACEPOINT_DEFINE
#include "mauve/tracing/mauve-tp.h"

namespace mauve {
  namespace tracing {

    void trace_start(const std::string& message) {
      tracepoint(mauve, start, message.c_str());
    }

    void trace_end(const std::string& message) {
      tracepoint(mauve, end, message.c_str());
    }

    void trace_event(const std::string& message) {
      tracepoint(mauve, event, message.c_str());
    }

    void trace_data(const std::string& name, const std::string& value) {
      tracepoint(mauve, data, name.c_str(), value.c_str());
    }

}}
