/*
 * Copyright 2017 ONERA
 *
 * This file is part of the MAUVE Tracing project.
 *
 * MAUVE Tracing is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * MAUVE Tracing is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with MAUVE.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
 */

#include <unistd.h>
#include <sstream>

#include "mauve/tracing.hpp"

int main(int argc, char const *argv[]) {
  for (int i = 0; i < 120; i++) {
    mauve::tracing::trace_start("comp_name");
    usleep(500*1000);
    std::stringstream ss; ss << i;
    mauve::tracing::trace_data("i", ss.str());
    mauve::tracing::trace_end("comp_name");
    usleep(1000*1000);
  }

  return 0;
}
